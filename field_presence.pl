#!/usr/bin/env perl
use strict;
use warnings;
use JSON;
use POSIX qw(strftime);
use Encode;
use 5.012;

my %fields = ();

sub walk_reg {
    my $fld = shift;
    my $json = shift;
    my $path = shift;
    my $pathstr = join(".", @{$path});

    my $ref_type = ref $json;

    if($ref_type eq "HASH") {
        foreach my $key (keys %{$json}) {
            walk_reg($fld, $json->{$key}, [ @{$path}, $key ]);
        }
    } elsif($ref_type eq "ARRAY") {
        foreach my $key (@{$json}) {
            walk_reg($fld, $key, [ @{$path}, '[]' ]);
        }
    } else {
        $fld->{$pathstr}++;
    }
}

## main
my $count = 0;
map {
    my $json = decode_json $_;
    walk_reg(\%fields, $json, []);
    $count++;
} <STDIN>;

printf "                %13s   %-2s  %s\n", "Count", "Ratio", "Field";
foreach my $f (sort {
        $fields{$b} <=> $fields{$a} || $a cmp $b
    } keys %fields) {
    printf "                %13d   %.2f   %s\n", $fields{$f}, $fields{$f}/$count, $f;
}
