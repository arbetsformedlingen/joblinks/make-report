FROM docker.io/library/ubuntu:22.04 AS base

WORKDIR /opt

ADD make-report.sh    /opt/make-report
ADD field_presence.pl /opt/field_presence.pl

RUN apt-get -y update &&\
    apt-get -y install jq libjson-perl &&\
    apt-get -y clean && apt-get -y autoremove

ENTRYPOINT [ "bash", "/opt/make-report" ]
