#!/bin/bash

inputdata=$(mktemp)

commit="$@"

cat - > "${inputdata}"

nr_ads=$(jq -s '. | length' < "${inputdata}")

echo -e "Pipeline host:\n\t\t$(hostname -f)"
echo
echo -e "Number of ads:\n\t\t${nr_ads}"
echo
echo -ne "Sum of job openings (field .originalJobPosting.totalJobOpenings):\n\t\t"
jq  '. | [ .originalJobPosting.totalJobOpenings ] ' < "${inputdata}" | awk '{sum+=$0} END{print sum}'
echo
echo "Top 20 ad sites (by ads, field .originalJobPosting.url):"
jq -r '. | .originalJobPosting.url' < "${inputdata}" \
    | sed -E 's|^(.*//[^/]+).*$|\1|' \
    | sort | uniq -c | sort -nr | head -n 20 \
    | sed 's|^|\t|'
echo
echo "Top 20 employers (by ads, field .originalJobPosting.hiringOrganization.name):"
jq -r '. | .originalJobPosting.hiringOrganization.name' < "${inputdata}" \
    | sort | uniq -c | sort -nr | head -n 20 \
    | sed 's|^|\t|'
echo
echo "Ads by scrapers (field .originalJobPosting.scraper):"
jq -r ' . | .originalJobPosting.scraper ' < "${inputdata}" \
    | sed -E 's|^(.*//[^/]+).*$|\1|' \
    | sort | uniq -c | sort -nr | sed 's|^|\t|'
echo

echo "Field presence:"
env LANG=C LC_ALL=C perl field_presence.pl < "${inputdata}"
echo

echo -e "Pipeline version:\n\t${commit}"
echo
